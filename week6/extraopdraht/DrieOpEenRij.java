package extraopdraht;

import java.util.Scanner;

public class DrieOpEenRij {
	
	private static char [][] bord = new char[3][3]; // Bord van 3 x 3
	private static String naamSpeler1 = "";
	private static String naamSpeler2 = "";
	private static char teken;
	private static int spelerAanZet = 1;
	
	public static void leegBord() {
		
		for(int i = 0; i < bord.length; i++) {
			
			for(int j = 0; j < bord.length; j ++) {
				
				bord[i][j] = '.';
				
			}
			
		}
		
	}
	
	public static void geefBordWeer() {
		
		for(int i = 0; i < bord.length; i++) {
			
			for(int j = 0; j < bord.length; j ++) {
				
				System.out.print(bord[i][j]);
				
			}
			
			System.out.println();
			
		}
		
		System.out.println();
		
	}
	
	public static boolean isBezet(int x, int y) {
		
		if(x < 1 || x > 3 || y < 1 || y > 3) {
			
			System.out.println("Deze coordinaten kunnen niet. Probeer het nog eens.");
			return true;
			
		}
		
		if(bord[x-1][y-1] != '.') {
			
			System.out.println("Het vakje is al gevuld");
			return true;
			
		}
		else {
			
			return false;
			
		}
		
	}
	
	public static void vulBord() {
		
		Scanner input = new Scanner(System.in);
		
		String huidigeSpeler = "";
		int x;
		int y;
		
		if(spelerAanZet == 1) {
			
			huidigeSpeler = naamSpeler1;
			teken = 'X';
			
		}
		else if(spelerAanZet == 2) {
			
			huidigeSpeler = naamSpeler2;
			teken = 'O';
			
		}
		
		do {
			
			System.out.println(huidigeSpeler + " plaats een " + teken + " op een coordinaat tussen 1 en 3");
			System.out.print("X: ");
			x = input.nextInt();
			System.out.print("Y: ");
			y = input.nextInt();
			System.out.println();
			
		} while(isBezet(y, y));
		
		bord[x-1][y-1] = teken;
		
		if(spelerAanZet == 1) {
			
			spelerAanZet = 2;
			
		}
		else if(spelerAanZet == 2) {
			
			spelerAanZet = 1;
			
		}
		
	}
	
	public static char checkBord() {
		
		if(bord[0][0] == 'O' && bord[0][1] == 'O' && bord[0][2] == 'O') { 
			
			return 'o';
			
		}
		else if(bord[1][0] == 'O' && bord[1][1] == 'O' && bord[1][2] == 'O') {
			
			return 'o';
			
		}
		else if(bord[2][0] == 'O' && bord[2][1] == 'O' && bord[2][2] == 'O') {
			
			return 'o';
			
		}
		else if(bord[0][0] == 'O' && bord[1][0] == 'O' && bord[2][0] == 'O') {
			
			return 'o';
			
		}
		else if(bord[0][1] == 'O' && bord[1][1] == 'O' && bord[2][1] == 'O') {
			
			return 'o';
			
		}
		else if(bord[0][2] == 'O' && bord[1][2] != 'O' && bord[2][2] == 'O') {
			
			return 'o';
			
		}
		else if(bord[0][0] == 'O' && bord[1][1] == 'O' && bord[2][2] == 'O') {
			
			return 'o';
			
		}
		else if(bord[0][2] == 'O' && bord[1][1] == 'O' && bord[2][0] == 'O') {
			
			return 'o';
			
		}
		
		if(bord[0][0] == 'X' && bord[0][1] == 'X' && bord[0][2] == 'X') { 
			
			return 'x';
			
		}
		else if(bord[1][0] == 'X' && bord[1][1] == 'X' && bord[1][2] == 'X') {
			
			return 'x';
			
		}
		else if(bord[2][0] == 'X' && bord[2][1] == 'X' && bord[2][2] == 'X') {
			
			return 'x';
			
		}
		else if(bord[0][0] == 'X' && bord[1][0] == 'X' && bord[2][0] == 'X') {
			
			return 'x';
			
		}
		else if(bord[0][1] == 'X' && bord[1][1] == 'X' && bord[2][1] == 'X') {
			
			return 'x';
			
		}
		else if(bord[0][2] == 'X' && bord[1][2] != 'X' && bord[2][2] == 'X') {
			
			return 'x';
			
		}
		else if(bord[0][0] == 'X' && bord[1][1] == 'X' && bord[2][2] == 'X') {
			
			return 'x';
			
		}
		else if(bord[0][2] == 'X' && bord[1][1] == 'X' && bord[2][0] == 'X') {
			
			return 'x';
			
		}
		else if(bord[0][0] != '.' && bord[0][1] != '.' && bord[0][2] != '.' && bord[1][0] != '.' && bord[1][1] != '.' && bord[1][2] != '.'
				&& bord[2][0] != '.' && bord[2][1] != '.' && bord[2][2] != '.') {
			
			return 'g';			
			
		}
		else {
			
			return 'a';
			
		}
		
	}

	public static void main(String[] args) {
		
		leegBord();
		
		Scanner input = new Scanner(System.in);
		System.out.print("Speler 1 naam: ");
		naamSpeler1 = input.nextLine();
		System.out.print("Speler 2 naam: ");
		naamSpeler2 = input.nextLine();
		
		do {
			
			vulBord();
			geefBordWeer();
			
		} while(checkBord() == 'a');
		
		if(checkBord() == 'x') {
			
			System.out.println(naamSpeler1 + " wint!");
			
		}
		else if(checkBord() == 'o') {
			
			System.out.println(naamSpeler2 + " wint!");
			
		}
		else if(checkBord() == 'g') {
			
			System.out.println("Niemand wint");
			
		}
		
		
	}

}
