package opdracht1;

import java.awt.Color;

public class Opdract1 {

	public static void main(String[] args) {
		
		Color bruin = new Color(156, 93, 28);
		
		Balk balk1 = new Balk();
		balk1.setLengte(40);
		balk1.setBreedte(14);
		balk1.setHoogte(3);
		balk1.setKleur(Color.blue);
		
		Balk balk2 = new Balk();
		balk2.setLengte(60);
		balk2.setBreedte(10);
		balk2.setHoogte(3);
		balk2.setKleur(Color.red);
		
		Balk balk3 = new Balk();
		balk3.setLengte(50);
		balk3.setBreedte(8);
		balk3.setHoogte(4);
		balk3.setKleur(Color.orange);
		
		Balk balk4 = new Balk();
		balk4.setLengte(70);
		balk4.setBreedte(24);
		balk4.setHoogte(16);
		balk4.setKleur(Color.MAGENTA);
		
		Balk balk5 = new Balk();
		balk5.setLengte(30);
		balk5.setBreedte(15);
		balk5.setHoogte(3);
		balk5.setKleur(Color.green);
		
		Balk arrBalken[] = new Balk[5];
		arrBalken[0] = balk1;
		arrBalken[1] = balk2;
		arrBalken[2] = balk3;
		arrBalken[3] = balk4;
		arrBalken[4] = balk5;
		
		Balk arrBalkenNieuw[] = new Balk[5];
		arrBalkenNieuw[0] = new Balk(balk1.getLengte(), balk1.getHoogte(), balk1.getBreedte(), balk1.getKleur());
		arrBalkenNieuw[1] = new Balk(balk2.getLengte(), balk2.getHoogte(), balk2.getBreedte(), balk2.getKleur());
		arrBalkenNieuw[2] = new Balk(balk3.getLengte(), balk3.getHoogte(), balk3.getBreedte(), balk3.getKleur());
		arrBalkenNieuw[3] = new Balk(balk4.getLengte(), balk4.getHoogte(), balk4.getBreedte(), balk4.getKleur());
		arrBalkenNieuw[4] = new Balk(balk5.getLengte(), balk5.getHoogte(), balk5.getBreedte(), balk5.getKleur());
		
		balk3.setHoogte(10);
		balk4.setHoogte(10);
		balk5.setHoogte(10);
		
		for(int i = 0; i < arrBalkenNieuw.length; i++) {
			
			arrBalkenNieuw[i].setKleur(bruin);
			
		}
		
		System.out.println(balk1.berekenGrondoppervlakte() + balk2.berekenGrondoppervlakte());
		
		arrBalken[0].setLengte(50);
		arrBalken[1].setLengte(50);
		
		System.out.println(balk1.berekenGrondoppervlakte() + balk2.berekenGrondoppervlakte());
		
		System.out.println();
		System.out.println("Volume balk1 t/m balk5: " + (balk1.berekenVolume() + balk2.berekenVolume() + balk3.berekenVolume()
							+ balk4.berekenVolume() + balk5.berekenVolume()));
		
		int volumeArrBalken = 0;
		for(int i = 0; i < 5; i++) {
			
			volumeArrBalken += arrBalken[i].berekenVolume();
			
		}
		
		System.out.println("Volume arrBalken: " + volumeArrBalken);
		
		int volumeArrBalkenNieuw = 0;
		for(int i = 0; i < 5; i++) {
			
			volumeArrBalkenNieuw += arrBalkenNieuw[i].berekenVolume();
			
		}
		
		System.out.println("Totaal volume arrBalkenNieuw: " + volumeArrBalkenNieuw);
		System.out.println();
		
		System.out.println("#\t Lengte\t Breedte\t Hoogte\t Kleur\t\t\t\tVolume\t Grondoppervlakte\t");
		
		for(int i = 0; i < 5; i++) {
			
			System.out.println((i+1) + "\t" + arrBalken[i].getLengte() + "\t" + arrBalken[i].getBreedte() + "\t\t" + arrBalken[i].getHoogte()
					+ "\t" + arrBalken[i].getKleur() + "\t" + arrBalken[i].berekenVolume() + "\t" + arrBalken[i].berekenGrondoppervlakte());
			
		}
		
		System.out.println();
		System.out.println("#\t Lengte\t Breedte\t Hoogte\t Kleur\t\t\t\tVolume\t Grondoppervlakte\t");
		
		for(int i = 0; i < 5; i++) {
			
			System.out.println((i+1) + "\t" + arrBalkenNieuw[i].getLengte() + "\t" + arrBalkenNieuw[i].getBreedte() + "\t\t" + arrBalkenNieuw[i].getHoogte()
					+ "\t" + arrBalkenNieuw[i].getKleur() + "\t" + arrBalkenNieuw[i].berekenVolume() + "\t" + arrBalkenNieuw[i].berekenGrondoppervlakte());
			
		}
		
		
		
	}

}
