package opdracht1;

import java.awt.Color;
import java.lang.reflect.Constructor;

public class Balk {

	private int lengte = 0;
	private int breedte = 0;
	private int hoogte = 0;
	private Color kleur = Color.WHITE;
	public final Color bruin = new Color(156, 93, 28);
	
	public Balk(int i, int j, int k, Color color) {
		
		setLengte(i);
		setHoogte(j);
		setBreedte(k);
		setKleur(color);
		
	}
	
	public Balk() {
		
	}

	public void setLengte(int waarde) {
		
		this.lengte = waarde;
		
	}
	
	public int getLengte() {
		
		return this.lengte;
		
	}
	
	public void setBreedte(int waarde) {
		
		this.breedte = waarde;
		
	}
	
	public int getBreedte() {
		
		return this.breedte;
		
	}
	
	public void setHoogte(int waarde) {
		
		this.hoogte = waarde;
		
	}
	
	public int getHoogte() {
		
		return this.hoogte;
		
	}
	
	public void setKleur(Color waarde) {
		
		this.kleur = waarde;
		
	}
	
	public Color getKleur() {
		
		return this.kleur;
		
	}
	
	public int berekenVolume() {
		
		return (this.breedte * this.lengte * this.hoogte);
		
	}
	
	public int berekenGrondoppervlakte() {
		
		return (this.breedte * this.lengte);
		
	}
	
}
