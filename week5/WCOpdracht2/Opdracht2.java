
package WCOpdracht2;

import java.util.Scanner;

public class Opdracht2
{

	public static void main(String[] args) {
		
		toonGetallen();

	}
	
	public static int[] voerInGetallen(int arrayLengte) {
		
		Scanner input = new Scanner(System.in);
		
		int ingevoerdeGetallen[] = new int[arrayLengte];
		
		for(int i = 0; i < ingevoerdeGetallen.length; i++) {
			
			System.out.print("Voer getal " + i + " in: ");
			ingevoerdeGetallen[i] = input.nextInt();
			
		}
		
		return ingevoerdeGetallen;
		
	}

	public static int bepaalMaximum(int[] ingevoerdeGetallen) {
		
		int maximum = 0;
		for (int i = 0; i < ingevoerdeGetallen.length; i++) {
			
			if(maximum < ingevoerdeGetallen[i]) {
				
				maximum = ingevoerdeGetallen[i];
				
			}
			
		}
		
		return maximum;
		
	}
	
	public static int bepaalMinimum(int[] ingevoerdeGetallen) {
		
		int minimum = ingevoerdeGetallen[0];
		for (int i = 0; i < ingevoerdeGetallen.length; i++) {
			
			if(minimum > ingevoerdeGetallen[i]) {
				
				minimum = ingevoerdeGetallen[i];
				
			}
			
		}
		
		return minimum;
		
		
	}

	public static int[] pakAlleEvenGetallen(int[] ingevoerdeGetallen) {
		
		int aantalHeleGetallen = 0;
		
		for(int i = 0; i < ingevoerdeGetallen.length; i++) {
			
			if(ingevoerdeGetallen[i] % 2 == 0) {
				
				aantalHeleGetallen++;
				
			}
			
		}
		
		int evenGetallen[] = new int[aantalHeleGetallen];
		
		for(int i = 0; i < ingevoerdeGetallen.length; i++) {
			
			if(ingevoerdeGetallen[i] % 2 == 0) {
				
				evenGetallen[i] = ingevoerdeGetallen[i];
				
			}
			
		}
		
		return evenGetallen;
		
	}
	
	public static void toonGetallen() {
		Scanner input = new Scanner(System.in);
		System.out.print("Hoeveel getallen opgeven? ");
		int arrayLengte = input.nextInt();
		
		int[] ingevoerdeGetallen = voerInGetallen(arrayLengte);
		int[] evenGetallen = pakAlleEvenGetallen(ingevoerdeGetallen);
		int maximum = bepaalMaximum(ingevoerdeGetallen);
		int minimum = bepaalMinimum(ingevoerdeGetallen);
		
		
		System.out.println("Het grootste getal is " + maximum);
		System.out.println("Het laagste getal is " + minimum);
		System.out.println("De even getallen zijn: ");
		
		for(int i = 0; i < evenGetallen.length; i++) {
			
			System.out.print(evenGetallen[i] + " ");
			
		}
		
	}
	
}
