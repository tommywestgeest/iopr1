package HWOpdracht2;

import java.util.Scanner;

public class Opgave2 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.print("Van hoeveel studenten wil je hun cijfers invoeren? ");
		int aantalStudenten = input.nextInt();
		
		int arrCijfersStudenten[][] = new int[aantalStudenten][]; 
		String arrNaam[] = new String[aantalStudenten];
		
		for(int i = 0; i < aantalStudenten; i++) {
			
			System.out.print("Wat is de naam van student " + (i+1) + "? ");
			arrNaam[i] = input.next();
			System.out.print("Hoeveel cijfers wil je invoeren van " + arrNaam[i] + "? ");
			int aantalCijfers = input.nextInt();
			arrCijfersStudenten[i] = new int[aantalCijfers];
			
			for(int j = 0; j < aantalCijfers; j++) {
				
				System.out.print("Geef cijfer " + (j+1) + " van " + aantalCijfers + ": ");
				arrCijfersStudenten[i][j] = input.nextInt();
				
			}
			
			System.out.println("");
			
		}
		
		for(int i = 0; i < aantalStudenten; i++) {
			
			System.out.print("Student " + (i+1) + " " + arrNaam[i] + ": ");
			
			for(int j = 0; j < arrCijfersStudenten[i].length; j++) {
				
				System.out.print(arrCijfersStudenten[i][j] + " ");
				
			}
			
			System.out.println();
			
		}

	}
	
}
