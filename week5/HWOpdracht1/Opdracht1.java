package HWOpdracht1;

import java.util.Collections;
import java.util.Scanner;

public class Opdracht1 {
	
	public static char[] maakArrayKleineLetters(String woord) {
		
		return woord.toLowerCase().toCharArray();
		
	}
	
	public static char [] maakReverse(char [] arrLetters) {
		
		char [] arrLettersReverse = new char[arrLetters.length];
		
		for(int i = 0; i < arrLetters.length; i++) {
			
			arrLettersReverse[arrLetters.length - 1 - i] = arrLetters[i];
			
		}
		
		return arrLettersReverse;
		
	}
	
	public static boolean isKlinker(char letter) {
		char [] arrKlinkers = {'a', 'e', 'o', 'u', 'i', 'y'};
		
		for(int i = 0; i < arrKlinkers.length; i++) {
			
			if(arrKlinkers[i] == letter) {
				
				return true;
				
			}
			
		}
		
		return false;
		
	}
	
	public static char [] verwijderKlinkers( char [] arrLetters) {
		
		char [] arrTemp = new char[arrLetters.length];
		int teller = 0;
		
		for(int i = 0; i < arrLetters.length; i++) {
			
			if(isKlinker(arrLetters[i]) == false) {
				
				arrTemp[teller] = arrLetters[i];
				teller += 1;
				
			}
			
		}
		
		char [] arrMedeklinkers = new char[teller];
		
		for(int i = 0; i < arrMedeklinkers.length; i++) {
			
			arrMedeklinkers[i] = arrTemp[i];
			
		}
		
		return arrMedeklinkers;
		
	}
	
	public static void toonZinUitLetters(char [] arrLetters) {
		
		for(int i = 0; i < arrLetters.length; i++) {
			
			System.out.print(arrLetters[i]);
			
		}
		
		System.out.println();
		
	}
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.print("Vul een woord in ");
		String woord = input.nextLine();
		
		char [] arrLetters = maakArrayKleineLetters(woord);
		char [] arrLettersReverse = maakReverse(arrLetters);
		char [] arrMedeklinkers = verwijderKlinkers(arrLetters);
		
		toonZinUitLetters(arrLetters);
		toonZinUitLetters(arrLettersReverse);
		toonZinUitLetters(arrMedeklinkers);
		
	}

}
