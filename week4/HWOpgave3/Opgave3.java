package HWOpgave3;

import java.util.Scanner;

public class Opgave3
{

	public static void main(String[] args)
	{
		
		Scanner input = new Scanner(System.in);
		System.out.print("Noem een  heel begingetal: ");
		int beginGetal = input.nextInt();
		System.out.print("Noem een heel eindgetal: ");
		int eindGetal = input.nextInt();
		
		if(beginGetal < eindGetal) {
			
			for(int i = beginGetal; i <= eindGetal; i++) {
				
				System.out.print(i+ "\t");
				System.out.print(formule1(i) + "\t");
				System.out.print(formule2(i) + "\t");
				System.out.print(formule3(i) + "\t");
				System.out.print(formule4(i) + "\t");
				System.out.println(formule5(i));
				
			}
		}
		else if(beginGetal > eindGetal) {
			
			for(int i = beginGetal; i >= eindGetal; i--) {
				
				System.out.print(i+ "\t");
				System.out.print(formule1(i) + "\t");
				System.out.print(formule2(i) + "\t");
				System.out.print(formule3(i) + "\t");
				System.out.print(formule4(i) + "\t");
				System.out.println(formule5(i));
				
			}
		}
	}

	public static int formule1(int x) {
		
		return (x + 2) * (x - 2);
		
	}
	
	public static int formule2(int x) {
		
		return (int) Math.pow(x, 2) + (5 * x) - 10;
		
	}
	
	public static int formule3(int x) {
		
		return (int) (Math.pow(x, 3) + (3 * ( x * x)) - ( 10 * x) + 16);
		
	}
	
	public static int formule4(int x) {
		
		return (int) (Math.pow(x, 3) + (5 * x) + 100) / ((4 * x) + 10);
		
	}
	
	public static int formule5(int x) {
		
		return (int) ((Math.pow(x, 5) + (4 * Math.pow(x, 3) - (5 * x) + 78)) * (Math.pow(x, 2) + ( 7 * x) - 6));
		
	}
	
}
