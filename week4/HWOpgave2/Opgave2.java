package HWOpgave2;

public class Opgave2
{
	
	public static void main(String[] args) {
		
		double gradenCelcius = 25;
		double gradenFahrenheit = 78;
		
		System.out.println(gradenCelcius + " celcius is " + celciusNaarFahrenheit(gradenCelcius) + " in Fahrenheit");
		System.out.println(gradenFahrenheit + " Fahrenheit is " + fahrenheitNaarCelcius(gradenFahrenheit) + " in celcius");
		
	}
	
	public static double celciusNaarFahrenheit(double gradenCelcius) {
		
		return  ((gradenCelcius * 1.8) + 32);
		
	}
	
	public static double fahrenheitNaarCelcius(double gradenFahrenheit) {
		
		return ((gradenFahrenheit - 32) / 1.8);
		
	}
	
}
