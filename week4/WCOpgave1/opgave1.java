package WCOpgave1;

public class opgave1
{
	public static void main(String[] args) {
		
		schrijfTabel(1000, 1100);
	
	}
	
	public static boolean isEven(int getal) {
		
		if(getal % 2 == 0) {
			
			return true;
		}
		else {
			
			return false;
			
		}
		
	}
	
	public static boolean isPriemgetal(int getal) {
		
		boolean isPriem = true;
		
		for(int i = 2; i < getal; i++) {
			
			if(getal % i == 0) {
				
				isPriem = false;
				break;
				
			}

		}
		
		if(isPriem) {
			
			return true;
			
		} 
		else {
			
			return false;
			
		}
		
	}
	
	public static void schrijfTabel(int beginGetal, int eindGetal) {
		
		System.out.println("Getal\t\t"+ "isEven\t\t" + "isPriem\t\t");
		
		for(int i = beginGetal; i <= eindGetal; i++) {
			
			System.out.println(i + "\t\t" + isEven(i) + "\t\t" + isPriemgetal(i));
			
		}
		
	}
	
}
