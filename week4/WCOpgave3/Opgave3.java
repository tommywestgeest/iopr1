package WCOpgave3;

import java.util.Scanner;

public class Opgave3
{

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		String woord = "";
		
		while(woord.length() < 5) {
		System.out.print("Geef een woord van minimaal 5 letter ");
		 woord = input.nextLine();
		 
		}		
		
		System.out.println(woord.toUpperCase());
		
		for(int i = 1; i < woord.length(); i++) {
			
			char c = woord.charAt(i);
			
			if(Character.isDigit(c)) {
				
				System.out.println(c + " is een cijfer");
				
			}
			else if(Character.isAlphabetic(c)) {
				
				System.out.println(c + " is een letter");
				
			}
			else {
				
				System.out.println(c + " is geen cijfer en geen letter");
				
			}
		}
	}
	

}
