package WCOpgave3;

import java.util.Scanner;

public class Opgave3D
{

	public static void main(String[] args)
	{
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Hoevel codes wil je genereren? ");
		int aantalCodes = input.nextInt();
		
		getAantalCodes(aantalCodes);
		int code = 0;
		
		for(int i = 1; i <= aantalCodes; i++) {
			
		do {
			
			code = (int) (Math.random() * 10000);
			
		}while(code < 1000 || code > 10000);
		
		System.out.println("Code " + i + ": " + code);
		
	}
}
	
	public static int getAantalCodes(int aantalCodes) {
		
		if(aantalCodes >= 10) {
			
			return 10;
			
		}
		else if(aantalCodes <= 1) {
			
			return 1;
			
		}
		else {
			
			return aantalCodes;
			
		}
	}
}
