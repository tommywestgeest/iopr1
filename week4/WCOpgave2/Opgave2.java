package WCOpgave2;

import java.util.Scanner;

public class Opgave2
{
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.print("Welk getal wil je kwadrateren? ");
		float getal = input.nextFloat();
		
		toonKwadraat(getal);
		
	}
	
	public static float kwadraat(float getal) {
		
		return getal * getal;
		
	}

	public static void toonKwadraat(float getal) {
		
		System.out.println("Het kwadraat van " + getal + " is " + kwadraat(getal));
		
	}
	
}
