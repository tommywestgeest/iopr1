package HWOpgave1;

import java.util.Scanner;

public class Opgave1A
{
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int kies = 0;
		
		do {
			
			geefOverzicht();
			
			System.out.print("Welk woord wil je weergeven?: ");
			kies = input.nextInt();
			
			switch(kies) {
						
			case 1: keuze1();
					break;
			case 2: keuze2();
					break;
			case 3: keuze3();
					break;
			case 4: keuze4();
					break;
			case 5: keuze5();
					break;
			default: break;
			
			
			}
			
		} while(kies != -1);
		
		System.out.println("Afgelopen.");
		
	}
	
	public static void keuze1() {
		
		System.out.println("Keuze 1");
		
	}
	
	public static void keuze2() {
		
		System.out.println("Keuze 2");
		
	}
	
	public static void keuze3() {
		
		System.out.println("Keuze 3");
		
	}

	public static void keuze4() {
		
		System.out.println("Keuze 4");
		
	}
	
	public static void keuze5() {
		
		System.out.println("Keuze 5");
		
	}
	
	public static void geefOverzicht() {
		
		System.out.println("De mogelijke keuzes zijn: ");
		System.out.println("1:\t\t Keuze 1");
		System.out.println("2:\t\t Keuze 2");
		System.out.println("3:\t\t Keuze 3");
		System.out.println("4:\t\t Keuze 4");
		System.out.println("5:\t\t Keuze 5");
		
	}
	
}
