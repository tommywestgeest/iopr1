package grootstegetal;

import java.util.Scanner;

public class GrootsteGetal
{

	public static void main(String[] args)
	{
		
		Scanner input = new Scanner(System.in);
		System.out.print("1: ");
		int getal1 = input.nextInt();
		System.out.print("2: ");
		int getal2 = input.nextInt();
		System.out.print("3: ");
		int getal3 = input.nextInt();
		
		System.out.println(getHigestNumber(getal1, getal2, getal3));

	}
	
	private static int getHigestNumber(int getal1, int getal2, int getal3)
	{
		
		int x = Math.max(getal1, getal2);
		return Math.max(x, getal3);
	}

}
