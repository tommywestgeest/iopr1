package WC1Opgave2;

public class iopr1_Werkcollegeopgave1
{

	public static void main(String[] args)
	{
		//Variabelen toewijzen
		byte a = 5;
		short b = 10;
		int c = 100;
		long d = 1000000;
		float e = 57;
		double f = 75.3;
		char g = 'T';
		boolean h = false;
		String i = "Er zijn in totaal";
		String j = "variabelen";
		
		System.out.println("Variabele a is: " + a);
		System.out.println("De som van " + b + " en " + c + " is " + (b + c));
		System.out.println(d + " gedeeld door " + e + " is " + (d / e));
		System.out.println(e + " vermenigvuldigd met " + f + " is " + (e * f));
		System.out.println(f + " afgetrokken van " + e + " is " + (e - f));
		System.out.println("De som van " + e + " en " + f + " is " + (e + f));
		System.out.println(c + " gedeeld door " + 7 + " is " + (c / 7));
		System.out.println(f + " - " + e + " = " + (f - e));
		System.out.println("Als je " + d + " deelt door " + e + " houd je als rest over: " + (d % e));
		System.out.println("De volgende letter na " + g + " is: U");
		System.out.println("De waarde van boolean h is: " + h);
		System.out.println(i + 10 + j);

	}

}
