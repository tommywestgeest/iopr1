package HW1Opgave2;

import java.util.Scanner;

public class Opgave2A
{
	public static void main(String[] args) {
		
		//Stelt Pi Vast
		final double PI = 3.141592653;

		Scanner input = new Scanner(System.in);
		
		System.out.println("Wat is de straal?");
		double straal = input.nextDouble();
		System.out.println("Wat is de hoogte?");
		double hoogte = input.nextDouble();
		
		double inhoud = (straal * straal) * PI * hoogte;
		System.out.println("De inhoud van deze cilinder is: " + inhoud);
		
	}

}
