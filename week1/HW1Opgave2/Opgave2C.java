package HW1Opgave2;

import java.util.Scanner;

public class Opgave2C
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("Hoeveel weeg je?");
		double kilo = input.nextDouble();
		System.out.println("Wat is je lengte in meters?");
		double lengte = input.nextDouble();
		
		double BMI = kilo / (lengte * lengte);
		System.out.println("Je hebt een BMI van: " + BMI);

	}

}
