package HW1Opgave2;

import java.util.Scanner;

public class Opgave2B
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("Wat is het eerste getal?");
		int getal1 = input.nextInt();
		System.out.println("Wat is het tweede getal?");
		int getal2 = input.nextInt();
		System.out.println("Wat is het derde getal?");
		int getal3 = input.nextInt();
		
		double gemiddelde = (getal1 + getal2 + getal3) / 3;
		System.out.println("Het gemiddelde is: " + gemiddelde);
		
		
	}

}
