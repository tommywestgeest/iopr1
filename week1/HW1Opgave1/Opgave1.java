package HW1Opgave1;

import java.util.Scanner;

public class Opgave1
{
	public static void main(String[] args) {
		
		//Hoevel seconden er in wat zitten
		final long SecondenInJaar = 365 * 24 * 60 * 60; 	// 31.536.000
		final long SecondenInDag = 24 * 60 * 60;			// 86.400
		final long SecondenInUur = 60 * 60;					// 3.600
		final long SecondenInMinuut = 60;					// 60
		
		Scanner input = new Scanner(System.in);
		
		//Vraagt om seconden
		System.out.println("Hoeveel seconden wil je omkeren?");
		String seconden = input.nextLine();
		
		//Zet de string om in een long
		long AantalSeconden = Long.parseLong(seconden);
		
		//Berekent seconden jaar
		long AantalJaar = AantalSeconden / SecondenInJaar;
		long SecondenResterend = AantalSeconden % SecondenInJaar;
		
		//Berekent seconden dag
		long AantalDagen = SecondenResterend / SecondenInDag;
		SecondenResterend %= SecondenInDag;
		
		//Berekent seconden uur
		long AantalUur = SecondenResterend / SecondenInUur;
		SecondenResterend %= SecondenInUur;
		
		//Berekent seconden minuut
		long AantalMinuten = SecondenResterend / SecondenInMinuut;
		SecondenResterend %= SecondenInMinuut;
		
		//Berekent seconden
		long SecondenOver = SecondenResterend;
		
		System.out.println("Dit zijn " + AantalJaar + " jaar/jaren;"); 
		System.out.println(AantalDagen + " dagen;");
		System.out.println(AantalUur + " uren;");
		System.out.println(AantalMinuten + " minuten;");
		System.out.println(SecondenOver + " seconden.");
		
	}
}
