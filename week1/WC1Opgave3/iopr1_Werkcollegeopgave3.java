package WC1Opgave3;

import java.util.Scanner;

public class iopr1_Werkcollegeopgave3
{
	public static void main(String[] args ) {
		
		//Vraagt om je naam
		System.out.println("Wat is je naam?");
		//Maakt scanner aan
		Scanner naam = new Scanner(System.in);
		//Maakt van je naam een string
		String name = naam.nextLine();
		//Print je naam
		System.out.println("Hallo " + name + " !");
		
	}
}
