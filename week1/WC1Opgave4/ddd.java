package WC1Opgave4;
import java.util.Scanner;

public class ddd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		final double prijs = 0.50;
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Hoeveel km wilt u reizen? ");
		int aantal_km = input.nextInt();
		System.out.print("Wat is uw leeftijd? ");
		int leeftijd = input.nextInt();
		
		double kosten;
		
		if (leeftijd < 12)
		{
			kosten = aantal_km * prijs * 0.5;
		}
		else if (leeftijd < 65)
		{
			kosten = aantal_km * prijs;
		}
		else
		{
			kosten = aantal_km * prijs * 0.75;
		}
		
		System.out.println("De prijs van deze rit is: €" + kosten);
	}
}