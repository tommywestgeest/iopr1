package WC1Opgave4;

import java.util.Scanner;


public class Opgave4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// De kostprijzen per product worden gedeclareerd en geinitialiseerd.
		final double kostprijsAppel = 0.50;
		final double kostprijsPeer = 0.70;
		final double kostprijsAardappel = 0.65;
		final double btw = 20.0;
		
		// De verkoopprijzen per producten worden hier berekend. 
		double verkoopprijsAppel = kostprijsAppel * (1.0 + btw/100.0);
		double verkoopprijsPeer = kostprijsPeer * (1.0 + btw/100.0);
		double verkoopprijsAardappel = kostprijsAardappel * (1.0 + btw/100.0);
				
		// Scanner-object wordt aangemaakt.
		Scanner input = new Scanner(System.in);

		// De naam wordt gesteld en de gebruiker kan zijn naam invoeren.  
		System.out.print("Wat is je naam? ");
		String naam = input.nextLine();
			
		// BTW-percentage wordt getoond.
		System.out.println("De BTW is: " + btw);
		
		// De gebruiker moet nu invoeren hoeveel appels hij wil kopen. 
		System.out.println("De verkoopprijs van een appel is: " + verkoopprijsAppel);
		System.out.print("Hoeveel appels wil je kopen? ");
		int aantalAppels = input.nextInt();
				
		// De gebruiker moet nu invoeren hoeveel peren hij wil kopen. 
		System.out.println("De verkoopprijs van een peer is: " + verkoopprijsPeer);
		System.out.print("Hoeveel peren wil je kopen? ");
		int aantalPeren = input.nextInt();

		// De gebruiker moet nu invoeren hoeveel aardappels hij wil kopen. 
		System.out.println("De verkoopprijs van een aardappel is: " + verkoopprijsAardappel);
		System.out.print("Hoeveel aardappels wil je kopen? ");
		int aantalAardappels = input.nextInt();

		System.out.println("");
		
		// Hieronder volgen alle nodige berekeningen.
		
		double totaalKostprijsAppels = aantalAppels * kostprijsAppel;
		double totaalKostprijsPeren = aantalPeren * kostprijsPeer;
		double totaalKostprijsAardappels = aantalAardappels * kostprijsAardappel;
		
		double totaleBTWAppels = btw/100.0 * totaalKostprijsAppels;
		double totaleOmzetAppels = totaalKostprijsAppels * (1+btw/100.0);
		double totaleBTWPeren = btw/100.0 * totaalKostprijsPeren;
		double totaleOmzetPeren = totaalKostprijsPeren * (1+btw/100.0);
		double totaleBTWAardappels = btw/100.0 * totaalKostprijsAardappels;
		double totaleOmzetAardappels = totaalKostprijsAardappels * (1+btw/100.0);
		
		double totaleOmzetAlles = totaleOmzetAppels + totaleOmzetPeren + totaleOmzetAardappels;
		double totaleBTW = totaleBTWAppels + totaleBTWPeren + totaleBTWPeren;
		
		// De berekeningen worden getoond. 
		System.out.println("Beste " + naam + ",");
		System.out.println();
		System.out.println("Hieronder volgt het betaaloverzicht.");
		System.out.println("====================================");
		System.out.println();
				
		System.out.println("Appels aantal:\t\t" + aantalAppels);
		System.out.println("Totaal kostprijs:\t" + totaalKostprijsAppels);
		System.out.println("Totaal BTW:\t\t" + totaleBTWAppels);
		System.out.println("Subtotaal omzet:\t" + totaleOmzetAppels);
		System.out.println();
		
		System.out.println("Peren aantal:\t\t" + aantalPeren);
		System.out.println("Totaal kostprijs:\t" + totaalKostprijsPeren);
		System.out.println("Totaal BTW:\t\t" + totaleBTWPeren);
		System.out.println("Subtotaal omzet:\t" + totaleOmzetPeren);
		System.out.println();
		
		System.out.println("Aardappels aantal:\t" + aantalAardappels);
		System.out.println("Totaal kostprijs:\t" + totaalKostprijsAardappels);
		System.out.println("Totaal BTW:\t\t" + totaleBTWAardappels);
		System.out.println("Subtotaal omzet:\t" + totaleOmzetAardappels);
		System.out.println();
		
		System.out.println("Totale omzet incl. BTW:\t\t" + totaleOmzetAlles);
		System.out.println("Totale BTW:\t\t\t" + totaleBTW);
	}
}