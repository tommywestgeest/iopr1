package HW2Opgave2;

import java.util.Scanner;

public class Opgave2
{
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Vul hieronder de a, b en c in:");
		System.out.println("a = ");
		double a = input.nextDouble();
		System.out.println("b = ");
		double b = input.nextDouble();
		System.out.println("c = ");
		double c = input.nextDouble();
		
		double D = (b * b) - (4 * a * c);
		
		if(D < 0) {
			//Bij een negative discriminant is er geen oplossing mogelijk
			System.out.println("Geen oplossing mogelijk");
		}
		else if(D == 0) {
			//De vergelijking heeft 1 oplossing
			double x = (-b + Math.sqrt(D)) / (2 * a);

			System.out.println("Er is 1 oplossing, namelijk x = " + x);
		}
		else if(D > 0) {
			//Er zijn 2 oplossingen mogelijk
			double x1 = (-b + Math.sqrt(D)) / (2 * a);
			double x2 = (-b - Math.sqrt(D)) / (2 * a);
			System.out.println("Er zijn 2 oplossingen mogelijk, namelijk x1 = " + x1 + " en  x2 = " + x2);
		}
		
		
	}
}
