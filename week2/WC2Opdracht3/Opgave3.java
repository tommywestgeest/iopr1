package WC2Opdracht3;

import java.util.Scanner;

public class Opgave3
{
	public static void main(String[] args) {
		
		final double KostPerKM = 0.50;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Hoeveel km wilt u reizen?");
		int KMReizen = input.nextInt();
		System.out.println("Wat is uw leeftijd?");
		int leeftijd = input.nextInt();
		
		double TotalePrijs = KMReizen * KostPerKM;
		double KinderPrijs = TotalePrijs * 0.50;
		double OuderPrijs = TotalePrijs * 0.75;
		
		if(leeftijd < 12) {
			System.out.println("De prijs van deze rit is: " + KinderPrijs);
		}
		else if(leeftijd > 65) {
			System.out.println("De prijs van deze rit is: " + OuderPrijs);
		}
		else {
			System.out.println("De prijs van deze rit is: " + TotalePrijs);
		}
		
	}
}
