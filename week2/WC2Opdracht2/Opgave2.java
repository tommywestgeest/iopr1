package WC2Opdracht2;

import java.util.Scanner;

public class Opgave2
{
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Van welke kleine letter wil je een hoofdletter maken?");
		char letter = input.nextLine().charAt(0);
		
		char hoofdletter;
		char klinker = 'k';
		
		switch(letter) {
		
		case 'a':	hoofdletter = 'A';
				 	klinker = 'k';
				 	break;	  
		case 'b':	hoofdletter = 'B';
		  		 	klinker = 'm';
		  		 	break;
		case 'c':	hoofdletter = 'C';
		 			klinker = 'm';
		 			break;	
		case 'd':	hoofdletter = 'D';
		 			klinker = 'm';
		 			break;	
		case 'e':	hoofdletter = 'E';
					klinker = 'k';
					break;	
		case 'f':	hoofdletter = 'F';
		 			klinker = 'm';
		 			break;	
		case 'g':	hoofdletter = 'G';
		 			klinker = 'm';
		 			break;	
		case 'h':	hoofdletter = 'H';
		 			klinker = 'm';
		 			break;	
		case 'i':	hoofdletter = 'I';
		 			klinker = 'k';
		 			break;	
		case 'j':	hoofdletter = 'J';
		 			klinker = 'm';
		 			break;	
		case 'k':	hoofdletter = 'K';
					klinker = 'm';
					break;	
		case 'l':	hoofdletter = 'L';
		 			klinker = 'm';
		 			break;	
		case 'm':	hoofdletter = 'M';
		 			klinker = 'm';
		 			break;	
		case 'n':	hoofdletter = 'N';
		 			klinker = 'm';
		 			break;	
		case 'o':	hoofdletter = 'O';
		 			klinker = 'k';
		 			break;	
		case 'p':	hoofdletter = 'P';
		 			klinker = 'm';
		 			break;	
		case 'q':	hoofdletter = 'Q';
		 			klinker = 'm';
		 			break;	
		case 'r':	hoofdletter = 'R';
		 			klinker = 'm';
		 			break;
		case 's':	hoofdletter = 'S';
		 			klinker = 'm';
		 			break;	
		case 't':	hoofdletter = 'T';
		 			klinker = 'm';
		 			break;	
		case 'u':	hoofdletter = 'U';
		 			klinker = 'k';
		 			break;	
		case 'v':	hoofdletter = 'V';
		 			klinker = 'm';
		 			break;	
		case 'w':	hoofdletter = 'W';
		 			klinker = 'm';
		 			break;	
		case 'x':	hoofdletter = 'X';
		 			klinker = 'm';
		 			break;	
		case 'y':	hoofdletter = 'Y';
		 			klinker = 'k';
		 			break;	
		case 'z':	hoofdletter = 'Z';
		 			klinker = 'm';
		 			break;	
		default:	hoofdletter = ' ';
					klinker = ' ';
					break;
					
		}
		
		switch(klinker) {
		
		case 'k':	System.out.println("De kleine letter is: " + letter);
					System.out.println("De hooftletter van " + letter + " is: " + hoofdletter);
					System.out.println("Letter " + hoofdletter + "is een klinker");
					break;
		case 'm':	System.out.println("De kleine letter is: " + letter);
					System.out.println("De hooftletter van " + letter + " is: " + hoofdletter);
					System.out.println("Letter " + hoofdletter + "is een medeklinker");
					break;
		case ' ':	System.out.println("Er is geen kleine letter ingevoerd");
					break;
		}
	}
}
