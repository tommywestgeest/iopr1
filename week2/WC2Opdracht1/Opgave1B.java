package WC2Opdracht1;

import java.util.Scanner;

public class Opgave1B
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("Geef een maandnummer: ");
		int MaandNummer = input.nextInt();
		
		switch(MaandNummer) {
			case 1: System.out.println("Januari");
					break;
			case 2: System.out.println("Februari");
					break;
			case 3: System.out.println("Maart");
					break;
			case 4: System.out.println("April");
					break;
			case 5: System.out.println("Mei");
					break;
			case 6: System.out.println("Juni");
					break;
			case 7: System.out.println("Juli");
					break;
			case 8: System.out.println("Augustus");
					break;
			case 9: System.out.println("September");
					break;
			case 10: System.out.println("Oktober");
					break;
			case 11: System.out.println("November");
					break;
			case 12: System.out.println("December");
					break;
			default: System.out.println("Dit is geen geldig naamdnummer");
					break;
		}

	}

}
