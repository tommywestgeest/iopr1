package WC2Opdracht1;

import java.util.Scanner;

public class Opgave1A
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("Geef een maandnummer op: ");
		int MaandNummer = input.nextInt();
		
		if(MaandNummer == 1) {
			System.out.println("Het is januari");
		}
		else if(MaandNummer == 2) {
			System.out.println("Het is februari");
		}
		else if(MaandNummer == 3) {
			System.out.println("Het is maart");
		}
		else if(MaandNummer == 4) {
			System.out.println("Het is april");
		}
		else if(MaandNummer == 5) {
			System.out.println("Het is mei");
		}
		else if (MaandNummer == 6) {
			System.out.println("het is juni");
		}
		else if (MaandNummer == 7) {
			System.out.println("het is juli");
		}
		else if (MaandNummer == 8) {
			System.out.println("het is augustus");
		}
		else if (MaandNummer == 9) {
			System.out.println("het is september");
		}
		else if (MaandNummer == 10) {
			System.out.println("het is oktober");
		}
		else if (MaandNummer == 11) {
			System.out.println("het is november");
		}
		else if (MaandNummer == 12) {
			System.out.println("het is december");
		}
		else {
			System.out.println("Dit is een verkeerd maandnummer");
		}

	}

}
