package HW2Opgave1;

import java.util.Scanner;

public class Opgave1
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("Noem een groot getal");
		int GrootGetal = input.nextInt();
		
		System.out.println("Noem een klein getal");
		int KleinGetal1 = input.nextInt();
		System.out.println("Noem nog een klein getal");
		int KleinGetal2 = input.nextInt();
		
		if(GrootGetal % KleinGetal1 == 0 && GrootGetal % KleinGetal2 == 0) {
			//Getal deelbaar door beide getallen
			System.out.println("Het grote getal is deelbaar door " + KleinGetal1 + 
					" en " + KleinGetal2);
		}
		else if(GrootGetal % KleinGetal1 == 0 && GrootGetal % KleinGetal2 != 0) {
			//Getal alleen deelbaar door het eerste getal
			System.out.println("Het grote getal is deelbaar door " + KleinGetal1 + 
					" maar niet deelbaar door " + KleinGetal2);
		}
		else if(GrootGetal % KleinGetal1 != 0 && GrootGetal % KleinGetal2 == 0) {
			//Getal alleen deelbaar door het tweede getal
			System.out.println("Het grote getal is niet deelbaar door " + KleinGetal1 + 
					" maar wel deelbaar door " + KleinGetal2);
		}
		else {
			//Getal niet deelbaar door beide getallen
			System.out.println("Het grote getal is niet deelbaar door " + KleinGetal1 + 
					" en ook niet deelbaar door " + KleinGetal2);
		}
		
	}
}
