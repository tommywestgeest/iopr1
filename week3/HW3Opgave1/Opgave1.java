


package HW3Opgave1;

import java.util.Scanner;

public class Opgave1
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("Priemgetallen tussen twee getallen uitrekenen");
		
		System.out.print("Noem een begingetal: ");
		int begin = input.nextInt();
		System.out.print("Noem een eindgetal: ");
		int eind = input.nextInt();
		
		if(begin < eind) {
			int nummer = begin;
			int priemOpRegel = 0;
			boolean priem = true;
			
			System.out.print("Hoeveel getallen per regels? ");
			int priemPerRegel = input.nextInt();
			
			while(nummer <= eind) {
			
			for(int deler = 2; deler < eind; deler++) {
				
				if(nummer % deler == 0) {
					priem = false;
					break;
				}
				
				if(priem) {
					priemOpRegel++;
					
					if(priemOpRegel % priemPerRegel == 0) {
						System.out.println(nummer);
					}
					else {
						System.out.print(nummer + " ");
					}
				}
				nummer++;
			}
		}
	}
	}
}
