package WC3Opgave3;

import java.util.Scanner;

public class Opgave3
{
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int Code1 = 0;
		
		do {
			System.out.println("Speler 1: vul een code van 2 cijfers in");
			Code1 = input.nextInt();
		} while(Code1 < 10 || Code1 > 99);
		
		int Code2 = 0;
		int Pogingen = 0;
		
		do {
			Pogingen++;
			
			System.out.println("Speler 2: probeer de code van spelder 1 te raden");
			System.out.println("Poging " + Pogingen);
			Code2 = input.nextInt();
			
			if(Code2 == -1) {
				System.out.println("Het codespel stopt!");
				break;
			}
			else if(Code2 < Code1) {
				System.out.println("Te laag!");
			}
			else if(Code2 > Code1) {
				System.out.println("Te hoog!");
			}
			
			
		} while (Code2 != Code1);
		
		if(Code2 == Code1) {
			System.out.println("Dit is juist, het koste je " + Pogingen + " pogingen.");
		}
	}
}
