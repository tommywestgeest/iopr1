package HW3Opgave2;

import java.util.Scanner;

public class Opgave2
{

	public static void main(String[] args)
	{
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Hoeveel woorden wil je invoeren om daarmee een zin te maken? ");
		int aantalWoorden = input.nextInt();
		
		String zin = "";
		String woord;
		
		for(int i = 1; i <= aantalWoorden; i++) {
			
			System.out.print("Woord " + i + ": ");
			woord = input.next();
			
			zin +=  woord;
			
			if(i < aantalWoorden) {
				
				zin += " ";
				
			}
		}
		
		System.out.println("De zin is geworden: ");
		System.out.println(zin);
		
	}
}
