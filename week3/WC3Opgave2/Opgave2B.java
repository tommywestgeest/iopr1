package WC3Opgave2;

import java.util.Scanner;

public class Opgave2B
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("Noem een beginjaar");
		int Begin = input.nextInt();
		System.out.println("Noem een eindjaar");
		int Eind = input.nextInt();
		
		if(Begin < Eind) {
			
			int AantalJaren = 0;
			String Schrikkeljaar = "";
			int i = Begin;
			
			while(i <= Eind) {
				
				if(i % 4 == 0) {
					
					Schrikkeljaar += " " + i;
					AantalJaren++;
					
					if(AantalJaren >= 10) {
						
						System.out.println(Schrikkeljaar);
						Schrikkeljaar = "";
						AantalJaren = 0;
						
					}
				}
				i++;
			}
			System.out.println(Schrikkeljaar);
		}
		else {
			System.out.println("Het begingetal mag niet groter zijn dan het eindgetal");
		}

	}

}
