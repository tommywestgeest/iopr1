package WC3Opgave2;

import java.util.Scanner;

public class Opgave2C
{
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Noem een beginjaar");
		int Begin = input.nextInt();
		System.out.println("Noem een eindjaar");
		int Eind = input.nextInt();
		
		int AantalJaar = 0;
		String Schrikkeljaren = "";
		int i = Begin;
		
		if(Begin < Eind) {
			
			do{
				
				if(i % 4 == 0) {
					AantalJaar++;
					Schrikkeljaren += " " + i;
					
					if(AantalJaar >= 10) {
						System.out.println(Schrikkeljaren);
						AantalJaar = 0;
						Schrikkeljaren = "";
					}
				}
				i++;
				
			}while(i <=Eind);
			System.out.println(Schrikkeljaren);
		}
		else {
			System.out.println("Het begingetal mag niet groter zijn dan het eindgetal");
		}
	}
}
