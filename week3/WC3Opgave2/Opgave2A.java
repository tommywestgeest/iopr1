package WC3Opgave2;

import java.util.Scanner;

public class Opgave2A
{
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Noem een beginjaar");
		int Begin = input.nextInt();
		System.out.println("Noem een eindjaar");
		int Eind = input.nextInt();
		
		if(Begin < Eind) {
			
			int AantalJaren = 0;
			String Schrikkeljaren = "";
			
			for(int i = Begin; i <= Eind; i++) {
				
				if(i % 4 == 0) {
					AantalJaren++;
					Schrikkeljaren += " " + i;
					
					if(AantalJaren >= 10) {
						System.out.println(Schrikkeljaren);
						Schrikkeljaren = "";
						AantalJaren = 0;
					}
				}
			}
			System.out.println(Schrikkeljaren);			
			
		}
		else {
			System.out.println("Het begingetal moet kleiner zijn dan het eindgetal");
		}
	}
}
