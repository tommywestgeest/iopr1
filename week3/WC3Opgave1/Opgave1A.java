package WC3Opgave1;

import java.util.Scanner;

public class Opgave1A
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.println("Van welk getal tussen de 1 en 100 wil je de tafel zien?");
		int TafelVan = input.nextInt();
		
		int i;
		
		if(TafelVan < 101 && TafelVan > 0) {
			for(i = 1; i < 11; i++) {
				System.out.println(i + " x " + TafelVan + " = " + (i * TafelVan));
			}
		}
		else {
			System.out.println(" Dit getal mag niet");
		}

	}

}
