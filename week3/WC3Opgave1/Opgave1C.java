package WC3Opgave1;

import java.util.Scanner;

public class Opgave1C
{
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Van welk getal tussen de 1 en 100 wil je de tafel zien?");
		int TafelVan = input.nextInt();
		
		int i = 1;
		
		if(TafelVan < 101 && TafelVan > 0) {
			do {
				System.out.println(i + " x " + TafelVan + " = " + (i * TafelVan));
				i++;
			}while(i < 11);
		}
		else {
			System.out.println("Dit getal mag niet");
		}
	}
}
