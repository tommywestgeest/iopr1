package WC3Opgave1;

import java.util.Scanner;

public class Opgave1B
{
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Van welk getal tussen de 1 en 100 wil je de tafel zien?");
		int TafelVan = input.nextInt();
		
		int i = 1;
		
		if(TafelVan < 101 && TafelVan  > 0) {
			while(i < 11) {
				System.out.println(i + " x " + TafelVan + " = " + (i * TafelVan));
				i++;
			}
		}
		else {
			System.out.println("Dit getal mag niet");
		}
		
	}
}
